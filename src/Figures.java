public class Figures {
        public Triangle triangle = new Triangle();
		public Rectangle rectangle = new Rectangle();
		public Circle circle = new Circle();
	    
        public Figures(){
        	this.circle.setRadius(0);
        	this.rectangle.setWidth(0);
        	this.rectangle.setLength(0);
        	this.triangle.setSide_a(0);
        	this.triangle.setSide_b(0);
        	this.triangle.setSide_c(0);
        }
        
        public static void main(String[] args) {
	    	Figures f=new Figures();
	    	// Rectangle test
	    	f.rectangle.setWidth(5);
	    	f.rectangle.setLength(7);
	        System.out.println("Rectangle width: " + f.rectangle.getWidth() + " and length: " + f.rectangle.getLength()
	                + "\nResulting area: " + f.rectangle.area()
	                + "\nResulting perimeter: " + f.rectangle.perimeter() + "\n");

	        // Circle test
	        f.circle.setRadius(5);
	        System.out.println("Circle radius: " + f.circle.getRadius()
	            + "\nResulting Area: " + f.circle.area()
	            + "\nResulting Perimeter: " + f.circle.perimeter() + "\n");

	        // Triangle test
	        f.triangle.setSide_a(5);
	        f.triangle.setSide_b(3);
	        f.triangle.setSide_c(4);
	        System.out.println("Triangle sides lengths: " + f.triangle.getSide_a() + ", " + f.triangle.getSide_b() + ", " + f.triangle.getSide_c()
	                + "\nResulting Area: " + f.triangle.area()
	                + "\nResulting Perimeter: " + f.triangle.perimeter() + "\n");
	    }

}
	
