
public class Rectangle extends Shape {
	private double width;
	private double length;

	public Rectangle() {
	}

	/**
	 * @return the width
	 */
	public double getWidth() {
		return width;
	}

	/**
	 * @param width the width to set
	 */
	public void setWidth(double width) {
		this.width = width;
	}

	/**
	 * @return the length
	 */
	public double getLength() {
		return length;
	}

	/**
	 * @param length the length to set
	 */
	public void setLength(double length) {
		this.length = length;
	}

	@Override
	public double area() {
		// A = w * l
		return getWidth() * getLength();
	}

	@Override
	public double perimeter() {
		// P = 2(w + l)
		return 2 * (getWidth() + getLength());
	}
}