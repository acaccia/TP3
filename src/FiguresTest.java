import static org.junit.Assert.*;

import org.junit.*;

public class FiguresTest {

	public Figures a;
	
	@Before
	public void setUp(){
		Figures a = new Figures();
	}
	
	@After
	public void setDown(){
		a.circle.setRadius(0);
		a.rectangle.setWidth(0);
		a.rectangle.setLength(0);
		a.triangle.setSide_a(0);
		a.triangle.setSide_b(0);
		a.triangle.setSide_c(0);
	}
	
	@Test
	public void testCircle_area() {
		a.circle.setRadius(1);
		assert a.circle.area() == a.circle.getPi();
	}

	@Test
	public void testCircle_perimeter() {
		a.circle.setRadius(1);
		assert a.circle.perimeter() == 2*a.circle.getPi();
	}

	@Test
	public void testRectangle_area() {
		a.rectangle.setWidth(1);
		a.rectangle.setLength(1);
		assert a.rectangle.area() == 1;
	}

	@Test
	public void testRectangle_perimeter() {
		a.rectangle.setWidth(1);
		a.rectangle.setLength(1);
		assert a.rectangle.perimeter() == 4;
	}

	@Test
	public void testTriangle_area() {
		a.triangle.setSide_a(4); a.triangle.setSide_b(5); a.triangle.setSide_c(6);
		assert a.triangle.area() == 20;
	}

	@Test
	public void testTriangle_perimeter() {
		a.triangle.setSide_a(4); a.triangle.setSide_b(5); a.triangle.setSide_c(6);
		assert a.triangle.perimeter() == 15;
	}

}
