
public class Circle extends Shape {
    private final static double pi = Math.PI;
	private double radius;

	public Circle() {
		setRadius(0);
	}

	/**
	 * @return the radius
	 */
	public double getRadius() {
		return radius;
	}

	/**
	 * @param radius the radius to set
	 */
	public void setRadius(double radius) {
		this.radius = radius;
	}

	/**
	 * @return the pi
	 */
	public double getPi() {
		return pi;
	}

	@Override
	public double area() {
	    // A = π r^2
	    return getPi() * Math.pow(getRadius(), 2);
	}

	@Override
	public double perimeter() {
	    // P = 2πr
	    return 2 * getPi() * getRadius();
	}
}