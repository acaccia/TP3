
public class Triangle extends Shape {
	private double side_a;
	private double side_b;
	private double side_c;

	public Triangle() {
	}

	/**
	 * @return the side_a
	 */
	public double getSide_a() {
		return side_a;
	}

	/**
	 * @param side_a the side_a to set
	 */
	public void setSide_a(double side_a) {
		this.side_a = side_a;
	}

	/**
	 * @return the side_b
	 */
	public double getSide_b() {
		return side_b;
	}

	/**
	 * @param side_b the side_b to set
	 */
	public void setSide_b(double side_b) {
		this.side_b = side_b;
	}

	/**
	 * @return the side_c
	 */
	public double getSide_c() {
		return side_c;
	}

	/**
	 * @param side_c the side_c to set
	 */
	public void setSide_c(double side_c) {
		this.side_c = side_c;
	}

	@Override
	public double area() {
		// Heron's formula:
	    // A = SquareRoot(s * (s - a) * (s - b) * (s - c)) 
	    // where s = (a + b + c) / 2, or 1/2 of the parameter of the triangle 
	    double s = (getSide_a() + getSide_b() + getSide_c()) / 2;
	    return Math.sqrt(s * (s - getSide_a()) * (s - getSide_b()) * (s - getSide_c()));
	}

	@Override
	public double perimeter() {
		// P = a + b + c
	    return getSide_a() + getSide_b() + getSide_c();
	}
}