
public abstract class Shape {

	public Shape() {
		super();
	}

	public abstract double perimeter();

	public abstract double area();

}